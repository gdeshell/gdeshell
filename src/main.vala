[SingleInstance]
public class CosmosShell : Object {

	// Singleton instance 
	public CosmosShell() {

	}

	public async void run_application (string []argv) {
		var av = argv.copy();

		try {
			SubprocessLauncher launcher = new SubprocessLauncher (SubprocessFlags.STDERR_PIPE);
			var process = launcher.spawnv(av);
			launcher.set_stderr_file_path(Environment.get_home_dir() + "/.cosmos-de/log_" + Path.get_basename(av[0]) + ".txt");
			all_process.add(process);
			yield process.wait_async();
			all_process.remove(process);
		}
		catch (Error e) {
			warning ("Cant run (%s)\n", e.message);
		}
	}

	public async void run() throws Error {
		this.run_dbus();
		this.run_wayfire();

		run_application.begin({"/usr/bin/cosmos-wallpaper"});
		run_application.begin({"/usr/bin/cosmos-menubar"});
		run_application.begin({"/usr/bin/cosmos-panel"});
		Thread.usleep(400000);
		run_application.begin({"/usr/bin/cosmos-menu"});


		onClosing.connect(() => Idle.add (run.callback));

		yield;

		foreach (var process in all_process){
			process.force_exit();
		}

		Process.spawn_command_line_sync("killall wayfire");
	}
	
	
	private void run_wayfire() throws Error {
		wayfire = new WayfireClient();
		wayfire.run();

		Bus.own_name (BusType.SESSION,
			"com.cosmos.shell",
			BusNameOwnerFlags.NONE,
			WayfireClientDBus.on_bus_aquired);
	}

	private void run_dbus(){
		Bus.own_name (BusType.SESSION,
			"com.cosmos.shell",
			BusNameOwnerFlags.NONE,
			CosmosShellDBus.on_bus_aquired);
	}

	public void close (){
		is_running = false;
		onClosing();
	}

	private signal void onClosing (); 
	private bool is_running = true; 
	private WayfireClient wayfire;
	private Gee.ArrayList<Subprocess> all_process = new Gee.ArrayList<Subprocess>();
}


/***
 * Main
 */
async void main() {
	try {
		Environment.set_variable("XDG_CURRENT_DESKTOP", "Cosmos", true);
		var shell = new CosmosShell();
		yield shell.run();
	}
	catch (Error e) {
		printerr("Error: %s\n", e.message);
	}
}
