namespace CosmosShellDBus {

	/**
	 * Callback for when the bus is acquired
	 */
	public void on_bus_aquired (DBusConnection conn) {
		try {
			var service = new CosmosShellDBus.Service ();
			conn.register_object ("/com/cosmos/shell", service);
		} catch (IOError e) {
			stderr.printf ("Could not register service: %s\n", e.message);
		}
	}

	/**
	 * CosmosShellDBus API
	 */
	[DBus(name = "com.cosmos.shell")]
	public class Service: Object {
		public void close_method() throws Error {
			var cosmos_shell = new CosmosShell();
			cosmos_shell.close();
		}
	}
}
