
namespace WayfireClientDBus { 
	
	/**
	 * Callback for when the bus is acquired
	 */
	public void on_bus_aquired (DBusConnection conn) {
		try {
			var service = new WayfireClientDBus.Service ();
			conn.register_object ("/com/cosmos/compositor", service);
		} catch (IOError e) {
			stderr.printf ("Could not register service: %s\n", e.message);
		}
	}


	/**
	 * WayfireClientDBus API
	 */
	[DBus (name = "com.cosmos.compositor")]
	public class Service : Object
	{
		public void expo_toggle() throws Error {
			var client = new WayfireClient ();
			client.send_command ("expo/toggle");
		}

		public void scale_toggle () throws Error {
			var client = new WayfireClient ();
			client.send_command ("scale/toggle");
		}
	}
}
