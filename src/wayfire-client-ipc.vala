/**
 * Client for the Wayfire compositor 
 * This is a simple client for the Wayfire compositor
 *
 * Connect the IPC wayfire's socket
 */
[SingleInstance]
public class WayfireClient : Object {
	private UnixSocketAddress socket;
	private SocketClient client;
	private SocketConnection connexion;
	private OutputStream output;
	private InputStream input;

	/**
	 * Create a new Wayfire client
	 */
	public WayfireClient() {
	}


	/**
	 * Connect to the Wayfire server
	 */
	public void run () throws Error {
		unowned string socket_path = Environment.get_variable("WAYFIRE_SOCKET");
		socket = new UnixSocketAddress(socket_path);
		client = new SocketClient();
		print ("Connected to the server\n");
		connexion = client.connect(socket);
		output = connexion.get_output_stream ();
		input = connexion.get_input_stream ();
		read_me.begin();
	}


	/**
	 * Signal emitted when a message is received from the server
	 *
	 * @param message The message received
	 * @param length The length of the message
	 */
	public signal void message_received (string message, int32 length);
	
	public signal void error_receive (string message);

	/**
	 * Send a command to the Wayfire server (optimized function)
	 *
	 * @param method_name The name of the method to call
	 * @param ... The arguments to pass to the method
	 *
	 * Example : send_command ("wm/action", "action", "close", "view-id", "0");
	 */
	public void send_command (string method_name, ...) throws Error {
		va_list args = va_list();
		uint8 buffer[8192];
		char *ptr = (char *)buffer;
		var method_len = method_name.length;

		/* Write the method name */
		Memory.copy (ptr, "{\"method\":\"", 11);
		ptr += 11;
		Memory.copy (ptr, method_name, method_len);
		ptr += method_len;
		ptr[0] = '"';
		ptr++;

		/* Write the arguments if exists */
		bool has_args = false;
		while (true) {
			string key = args.arg<string>();
			if (key == null) {
				if (has_args) {
					ptr[0] = '}';
					ptr++;
				}
				break;
			}

			string val = args.arg<string>();
			if (has_args == false) {
				Memory.copy (ptr, ",\"data\":{", 9);
				ptr += 9;
				has_args = true;
			}
			else {
				ptr[0] = ',';
				ptr++;
			}

			var key_len = key.length;
			ptr [0] = '"';
			ptr++;

			Memory.copy (ptr, key, key_len);
			ptr += key_len;

			Memory.copy(ptr, "\":", 2);
			ptr += 2;
			var val_len = val.length;
			Memory.copy (ptr, val, val_len);
			ptr += val_len;
		}
		ptr[0] = '}';


		int32 len = (int32)(ptr - (char *)buffer + 1);
		uint8 len_ptr[4];

		Memory.copy (len_ptr, &len, 4);
		// write 4 octet (int32) length of the message
		output.write(len_ptr);
		// write the message
		output.write(buffer[0:len]);
	}


	private async void read_me () {
		var buffer = new uint8[32768];

		while (true) {
			print ("Reading from the server\n");
			try {
				ssize_t result = 0;
				char *str;

				do {
					result = yield input.read_async(buffer[0:32768]);
					buffer[result] = '\0';
					int32 len = 0;
					Memory.copy (&len, buffer, 4);
					str = (char *)buffer;
					str += 4;
					message_received.emit ((string)str, len);
				}
				while(result > 0);
			}
			catch (Error e) {
				Error error = e;
				Idle.add(() => {
					error_receive.emit(error.message);
					return false;
				});
				break;
			}
		}
		print ("closing reading\n");
	}
}
